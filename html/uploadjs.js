
var // where files are dropped + file selector is opened
	dropRegion = document.getElementById("drop-region"),
	// where images are previewed
	filePreviewRegion = document.getElementById("file-preview");


// open file selector when clicked on the drop region
var fakeInput = document.createElement("input");
fakeInput.type = "file";
fakeInput.accept = "image/*";
fakeInput.multiple = true;
dropRegion.addEventListener('click', function() {
	fakeInput.click();
});

fakeInput.addEventListener("change", function() {
	var files = fakeInput.files;
	handleFiles(files);
});


function preventDefault(e) {
	e.preventDefault();
  	e.stopPropagation();
}

dropRegion.addEventListener('dragenter', preventDefault, false)
dropRegion.addEventListener('dragleave', preventDefault, false)
dropRegion.addEventListener('dragover', preventDefault, false)
dropRegion.addEventListener('drop', preventDefault, false)


function handleDrop(e) {
	var dt = e.dataTransfer,
		files = dt.files;

	if (files.length) {

		handleFiles(files);
		
	} else {

		// check for img
		var html = dt.getData('text/html'),
	        match = html && /\bsrc="?([^"\s]+)"?\s*/.exec(html),
	        url = match && match[1];



	    if (url) {
	        uploadImageFromURL(url);
	        return;
	    }

	}


	function uploadImageFromURL(url) {
		var img = new Image;
        var c = document.createElement("canvas");
        var ctx = c.getContext("2d");

        img.onload = function() {
            c.width = this.naturalWidth;     // update canvas size to match image
            c.height = this.naturalHeight;
            ctx.drawImage(this, 0, 0);       // draw in image
            c.toBlob(function(blob) {        // get content as PNG blob

            	// call our main function
                handleFiles( [blob] );

            }, "image/png");
        };
        img.onerror = function() {
            alert("Error in uploading");
        }
        img.crossOrigin = "";              // if from different origin
        img.src = url;
	}

}

dropRegion.addEventListener('drop', handleDrop, false);



function handleFiles(files) {
	for (var i = 0, len = files.length; i < len; i++) {
		if (validateImage(files[i]))
			previewAnduploadImage(files[i]);
	}
}

function validateImage(image) {
	// check the type
	var validTypes = ['image/jpeg', 'image/png', 'image/gif', 'image/tiff'];
	if (validTypes.indexOf( image.type ) === -1) {
		alert("Invalid File Type");
		return false;
	}

	// check the size
	var maxSizeInBytes = 10e6; // 10MB
	if (image.size > maxSizeInBytes) {
		alert("File too large");
		return false;
	}

	return true;

}

function previewAnduploadImage(image) {
	// container
	var fileUpload = document.createElement("div");
	fileUpload.className = "file-view";
	filePreviewRegion.appendChild(fileUpload);

	// Showing name
	var fileName = document.createElement("p");
	fileName.innerHTML = image.name
	fileName.className = "file-name";
	fileUpload.appendChild(fileName);

	// Showing filesize
	var fileSize = document.createElement("p");
	fileSize.innerHTML = '<b>' + Math.round((image.size / 1024)) + '</b> KB'
	fileSize.className = "file-size";
	fileUpload.appendChild(fileSize);

	// Showing meter bar
	let spanId = image.name + document.getElementsByClassName('file-view').length
	var meter = document.createElement("div");
	meter.innerHTML = '<span class="orange" id=' + spanId + ' style="width: 0%"></span>'
	meter.className = "meter";
	fileUpload.appendChild(meter);
	
	var url = document.createElement("a");
	fileUpload.appendChild(url);

	// read the image...
	var reader = new FileReader();
	// reader.onload = function(e) {
	// 	img.src = e.target.result;
	// }
	reader.readAsDataURL(image);
	document.getElementById("result-container").style.display = "block";


    var xhr = new XMLHttpRequest();
    xhr.open('POST', window.location.protocol + '/convert');
	xhr.setRequestHeader('Content-Type', "application/octet-stream");

	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status === 200) {
				// done!
				console.log(xhr.response);
				document.getElementById(spanId).className = "green"
				var json = JSON.parse(xhr.responseText)
				var imageUrl = json.output.URL.split("/")

				url.href = window.location.protocol + "/images/" + imageUrl[4]
				url.innerText="Open";
				url.target="_blank"
				var bytesSaved = parseInt(document.getElementById("bytesSaved").innerText, 10) + (((json.input.Size - json.output.Size)*100)/100)
				document.getElementById("bytesSaved").innerText = bytesSaved
				document.getElementById("output-container").style.display = "block";
				fileSize.innerHTML = '<b>-' + Math.round((1 - json.output.Ratio)*100) + '%</b>'
				fileUpload.className = fileUpload.className + " clickable"
				$(".clickable").click(function() {
					//window.location = $(this).find("a").attr("href"); 
					let url = $(this).find("a").attr("href");
					window.open(url, '_blank');
					
					return false;
				  });
				

				
			} else {
				// error!
		}
	}

	xhr.upload.onprogress = e => {
		let perc = (e.loaded / e.total * 100) || 100
		console.log(spanId)




		setTimeout(() => {
			let x = $(`#${$.escapeSelector(spanId)}`);
			console.log(x, perc);
			x.animate({
				width: `${perc}%` 
			}, 500)    
		}, 0)
	}


	xhr.send(image);

}
