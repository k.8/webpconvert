FROM alpine
WORKDIR /usr/src/goapp
COPY ./lib/ ./lib/
COPY ./html/ ./html/
COPY webserv .
COPY ./images/index.html ./images/index.html


RUN mkdir ./uploads
RUN apk update
RUN apk add libwebp-tools

EXPOSE 8000
CMD ["./webserv"]
