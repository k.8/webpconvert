package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/google/uuid"
	"golang.org/x/image/tiff"
)

//curl ifconfig.me
type reply struct {
	Input  inputInfo  `json:"input"`
	Output outputInfo `json:"output"`
}
type inputInfo struct {
	Size     int64
	Filetype string
}

type outputInfo struct {
	Size     int64
	Filetype string
	Ratio    float64
	URL      string
}

type job struct {
	w    http.ResponseWriter
	r    *http.Request
	body []byte
	u    uuid.UUID
}
type allowedMimes struct {
	mimes []string
}

var mimes = allowedMimes{[]string{"image/jpeg", "image/gif", "image/tiff", "image/png"}}

func findFiletype(body []byte) (string, error) {
	//allowedMimes := []string{"image/jpeg", "image/gif", "image/tiff", "image/png"}
	imgType := http.DetectContentType(body)
	for _, mimeTypes := range mimes.mimes {
		if imgType == mimeTypes {
			return "." + strings.Split(imgType, "/")[1], nil
		}
	}
	return "", errors.New("The image format is not supported")
}

func processImg(w http.ResponseWriter, r *http.Request, body []byte, u uuid.UUID) {
	filetype, err := findFiletype(body)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	targetSize := r.Header.Get("Target-Size")
	if targetSize == "" {
		targetSize = "0"
	}
	err = saveImage(body, filetype, u)
	if err != nil {
		w.Write([]byte("Failed to save the image" + err.Error()))
		return
	}
	err = convertToWebp(u, filetype, targetSize)
	if err != nil {
		w.Write([]byte("Failed to convert the image" + err.Error()))
		return
	}
	response, err := calculateData(u, filetype, r.Header.Get("Referer"))
	if err != nil {
		w.Write([]byte("Failed to calculate the data" + err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	enableCors(&w)
	w.Write(response)

	defer func() {
		err = deleteFile(u, filetype)
		if err != nil {
			w.Write([]byte("Failed to delete the file" + err.Error()))
		}
	}()
}

// TryEnqueue tries to enqueue a job to the given job channel. Returns true if
// the operation was successful, and false if enqueuing would not have been
// possible without blocking. Job is not enqueued in the latter case.
func TryEnqueue(w http.ResponseWriter, r *http.Request, JobChan chan<- job, u uuid.UUID) {
	body, _ := ioutil.ReadAll(r.Body)

	job := job{w, r, body, u}
	select {
	case JobChan <- job:
	default:
		http.Error(w, "Not enough resources on server", 503)
		return
	}
}

func worker(jobChan <-chan job, m map[uuid.UUID]bool) {
	for job := range jobChan {
		processImg(job.w, job.r, job.body, job.u)
		m[job.u] = true
	}
}

func saveImage(body []uint8, filetype string, u uuid.UUID) error {
	if filetype == ".gif" {
		img, err := gif.DecodeAll(bytes.NewReader(body))
		if err != nil {
			return err
		}
		f, err := os.Create("./uploads/" + u.String() + filetype)
		defer f.Close()
		if err != nil {
			return err
		}
		err = gif.EncodeAll(f, img)
		if err != nil {
			return err
		}
	} else {
		img, _, err := image.Decode(bytes.NewReader(body))
		if err != nil {
			return err
		}
		f, err := os.Create("./uploads/" + u.String() + filetype)
		if err != nil {
			return err
		}

		defer f.Close()

		if filetype == ".jpeg" {
			err = jpeg.Encode(f, img, nil)
		} else if filetype == ".png" {
			err = png.Encode(f, img)
		} else if filetype == ".tiff" {
			err = tiff.Encode(f, img, nil)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func convertToWebp(u uuid.UUID, filetype string, targetSize string) error {

	command := "cwebp"
	arguments := []string{"-m", "4", "-size", targetSize, "-sharp_yuv", "-alpha_filter", "best", "uploads/" + u.String() + filetype, "-o", "images/" + u.String() + ".webp"}
	if filetype == ".gif" {
		command = "gif2webp"
		arguments = []string{"-mixed", "-min_size", "-metadata", "none", "-m", "4", "uploads/" + u.String() + filetype, "-o", "images/" + u.String() + ".webp"}
	}
	cmd := exec.Command(command, arguments...)

	err := cmd.Run()
	return err
}

func calculateData(u uuid.UUID, filetype string, referer string) ([]byte, error) {
	originalFile, err := os.Stat("uploads/" + u.String() + filetype)
	if err != nil {
		return nil, err
	}
	newFile, err := os.Stat("images/" + u.String() + ".webp")
	if err != nil {
		return nil, err
	}
	// get the size
	originalFileSize := originalFile.Size()
	newFileSize := newFile.Size()
	var ratio float64 = (float64(newFileSize) / float64(originalFileSize))
	url := referer + "images/" + u.String() + ".webp"
	input := inputInfo{originalFileSize, filetype}
	output := outputInfo{newFileSize, ".webp", math.Round(ratio*100) / 100, url}
	m := reply{input, output}
	b, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}

	return b, nil

}

func deleteFile(u uuid.UUID, filetype string) error {
	err := os.Remove("uploads/" + u.String() + filetype)
	if err != nil {
		return err
	}
	return nil
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func main() {
	JobChan := make(chan job, 100)

	// start the worker
	m := make(map[uuid.UUID]bool)
	go worker(JobChan, m)
	fs := http.FileServer(http.Dir("./html"))
	http.Handle("/", http.StripPrefix("/", fs))

	http.HandleFunc("/convert", func(w http.ResponseWriter, r *http.Request) {
		u, _ := uuid.NewRandom()
		//body, _ := ioutil.ReadAll(r.Body)
		TryEnqueue(w, r, JobChan, u)
		complete := false
		enableCors(&w)

		for i := 0; i < 60; i++ {
			for key, element := range m {
				if key == u && element == true {
					delete(m, u)
					complete = true
					return
				}
			}
			time.Sleep(1 * time.Second)
		}
		if !complete {
			w.Write([]byte("The request took too long to run on the server"))
		}
	})

	http.HandleFunc("/images/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	http.HandleFunc("/allowed/", func(w http.ResponseWriter, r *http.Request) {
		b, err := json.Marshal(mimes.mimes)
		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(b)
	})

	log.Fatal(http.ListenAndServe(":8000", nil))

}
